module.exports = {
  root: true,
  env: {
    "es6": true,
  },
  rules: {
    'semi': [2, "always"],
    'vue/name-property-casing': 'off',
    'vue/prop-name-casing': 'off',
    'no-console': 'off',
    'max-len': ['error', { 'code': 100 }],
    'no-unused-vars': ['error', { 'argsIgnorePattern': '[uU]nused',
                                  'varsIgnorePattern': '[uU]nused' }],
  },
};