/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2024 Razer <razerraz@free.fr>
* Copyright 2014 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

/* eslint-disable no-unused-vars */
import St from 'gi://St';
import Gio from 'gi://Gio';
import Shell from 'gi://Shell';
import * as Config from 'resource:///org/gnome/shell/misc/config.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

export const shellVersion = Config.PACKAGE_VERSION.split('.').map(s => Number(s))[0];

export function get_settings_app(core_config) {
  let desktop_file = 'bubblemail-settings.desktop';
  if (Object.prototype.hasOwnProperty.call(core_config, 'version')) {
    let service_version = core_config.version.replace('beta', '');
    if (parseFloat(service_version.replace('alpha', '')) > 0.3) {
      desktop_file = 'bubblemail.desktop';
    }
    return desktop_file;
  }
  if (!Shell.AppSystem.get_default().lookup_app(desktop_file)) {
    desktop_file = 'bubblemail.desktop';
    if (!Shell.AppSystem.get_default().lookup_app(desktop_file)) return null;
    return desktop_file;
  }
  return desktop_file;
}

export function get_screen_width() {
  return global.display.get_monitor_geometry(
    global.display.get_primary_monitor()).width || 1024;
}

export function run_desktop_app(desktopFile) {
  if (!desktopFile) return;
  let desktop_app = Shell.AppSystem.get_default().lookup_app(desktopFile);
  if (!desktop_app) return;
  desktop_app.activate();
}

export function get_mail_app() {
  let mail_app = Gio.AppInfo.get_default_for_type('x-scheme-handler/mailto', false);
  if (!mail_app) return;
  return mail_app.get_id();
}

export function get_default_avatar(options) {
  if (options.default_avatar) return options.default_avatar;
  let icon_theme;
  icon_theme = new St.IconTheme();
  let icon_info = icon_theme.lookup_icon('avatar-default', 48, 0);
  if (!icon_info) return null;
  options.default_avatar = icon_info.get_filename();
  return options.default_avatar;
}
