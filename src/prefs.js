/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2024 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

import Gtk from 'gi://Gtk?version=4.0';
import Adw from 'gi://Adw';
import Gio from 'gi://Gio';

import {ExtensionPreferences, gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

import * as Opts from './opts.js';

export default class BubblemailPrefs extends ExtensionPreferences {

  fillPreferencesWindow(window) {
    window.settings = this.getSettings();
    const page = new Adw.PreferencesPage({
        title: _('Preferences'),
        icon_name: 'dialog-information-symbolic',
    });
    window.add(page);
    const general_group = new Adw.PreferencesGroup({
      title: _('General'),
    });
    page.add(general_group);
    this.addSwitchRow(window, general_group, _('Always Show'),
                      _('Show the indicator when maillist is empty'), Opts.KEY.ALWAYS_SHOW);
    this.addSwitchRow(window, general_group, _('Mail count animation'), _('Animate mail count label'),
                      Opts.KEY.ANIMATE_COUNTER);
    this.addSwitchRow(window, general_group, _('Counter over the icon'),
                      _('Show mail counter over the mail icon'), Opts.KEY.COUNTER_OVER_ICON);
    this.addSwitchRow(window, general_group, _('Menu icons'), _('Show icons for the menu items'),
                      Opts.KEY.SHOW_MENU_ICONS);
    this.addSwitchRow(window, general_group, _('Mailer launcher'), _('Show mailer application launcher'),
                      Opts.KEY.SHOW_MENU_MAILAPP);
    const mail_group = new Adw.PreferencesGroup({title: _('Mail list')});
    page.add(mail_group);
    this.addSwitchRow(window, mail_group, _('Newest at first'),
                      _('Show newest mails at first'), Opts.KEY.NEWEST_FIRST);
    this.addSwitchRow(window, mail_group, _('Group by account'),
                      _('Group mails by account'), Opts.KEY.GROUP_BY_ACCOUNT);
    this.addSwitchRow(window, mail_group, _('Show avatars'),
                      _('Show avatar from email sender'), Opts.KEY.SHOW_AVATARS);
    const spinrow = Adw.SpinRow.new_with_range(0, 50, 5);
    spinrow.set_title(_('Automatically hide subjects'));
    spinrow.set_subtitle(
      _('Hide subjects when mail list exceeds (Subjects will be always visible if set to 0)'));
    spinrow.set_numeric(true);
    spinrow.set_value(window.settings.get_int(Opts.KEY.MIN_HIDE_SUBJECT));
    spinrow.connect('changed', (spinrow) => {
      window.settings.set_int(Opts.KEY.MIN_HIDE_SUBJECT, spinrow.value);
      Gio.Settings.sync();
    });
    mail_group.add(spinrow);
    this.addSwitchRow(window, mail_group, _('Show dates'),
                      _('Show date of email send'), Opts.KEY.SHOW_DATES);
  }

  addSwitchRow(window, group, title, subtitle, optKey) {
    const row = new Adw.SwitchRow({title: title, subtitle: subtitle});
    row.set_active(window.settings.get_boolean(optKey));
    row.connect('notify::active' , (row) => {
      window.settings.set_boolean(optKey, row.active);
      Gio.Settings.sync();
    });
    group.add(row);
  }
}
