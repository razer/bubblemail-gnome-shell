/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2024 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

import Clutter from 'gi://Clutter';
import St from 'gi://St';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import { formatTime } from 'resource:///org/gnome/shell/misc/dateUtils.js';
import * as Util from 'resource:///org/gnome/shell/misc/util.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';
import * as Utils from './utils.js';

export let MailItem = GObject.registerClass(  // eslint-disable-line no-unused-vars
class MailItem extends PopupMenu.PopupBaseMenuItem {

  _init(mail, options, extension) {
    super._init();
    this.hover = false;
    this.can_focus = false;
    this.mail = mail;
    this.options = options;
    this.extension = extension;
    this.date_lbl = null;
    this.icon = null;
    this.close_btn = null;
    this.error = 0;
    this.show_subject = (!options.min_hide_subject
                         || this.extension.mail_map.length < options.min_hide_subject);
    this.add_style_class_name('mail-item');
    this.connect('notify::hover', this.on_hover.bind(this));
    let datetime = parseInt(mail['datetime']);
    let hbox = new St.BoxLayout({x_expand: true, reactive: true, style_class: 'mail-item-box',
                                 style: 'min-width:%spx; max-width:%spx;'.format(
                                 parseInt(this.options.max_width/1.33), this.options.max_width)});
    if (this.options.group)
        hbox.add_style_class_name('mail-item-box-grouped');
    if (this.options.show_avatars) {
      let default_avatar = Utils.get_default_avatar(this.options);
      let avatar = (mail.avatar && mail.avatar.length) ? mail.avatar : default_avatar;
      this.icon = new St.Bin({
        style_class: 'avatar', style: 'background-image: url("%s");'.format(avatar),
        y_expand: true});
      hbox.add_child(this.icon);
    }

    let vbox = new St.BoxLayout({vertical: true, x_expand: true});
    let name_lbl = new St.Label({text: mail.name.length ? mail.name : mail.address,
                                 x_expand: true, style_class: 'message-title from'});
    name_lbl.clutter_text.single_line_mode= true;
    let hbox2 = new St.BoxLayout({vertical: false, x_expand: true});
    hbox2.add_child(name_lbl);
    if (this.options.show_date) {
      let gdatetime = GLib.DateTime.new_from_unix_local(datetime);
      this.date_lbl = new St.Label({
        text: formatTime(gdatetime), style_class: 'no-networks-label date'});
      hbox2.add_child(this.date_lbl);
    }
    this.close_btn = new St.Button({y_align: Clutter.ActorAlign.START, visible: false});
    this.close_btn.connect('clicked', () => {
      this.hide();
      this.extension.dismiss(mail.uuid);
    });
    this.close_btn.child = new St.Icon({icon_size: 16, icon_name: 'window-close-symbolic'});
    hbox2.add_child(this.close_btn);
    vbox.add_child(hbox2);
    if (this.show_subject) {
      let subject = new St.Label({text: mail.subject, style_class: 'no-networks-label subject'});
      subject.clutter_text.single_line_mode= true;
      vbox.add_child(subject);
    }
    hbox.add_child(vbox);

    hbox.connect('button-release-event', (actor, event) => {
      this.open_mail();
      this.activate(event);
      return Clutter.EVENT_STOP;
    });

    hbox.connect('touch-event', (actor, event) => {
      if (event.type() != Clutter.EventType.TOUCH_END)
        return Clutter.EVENT_PROPAGATE;
      this.open_mail();
      this.activate(event);
      return Clutter.EVENT_STOP;
    });

    hbox.connect('key-press-event', (actor, event) => {
      let symbol = event.get_key_symbol();
      if (symbol == Clutter.KEY_space || symbol == Clutter.KEY_Return) {
        this.open_mail();
        this.activate(event);
        return Clutter.EVENT_STOP;
      }
      if (symbol == Clutter.KEY_Delete) {
        this.hide();
        this.extension.dismiss(this.uuid);
        return Clutter.EVENT_STOP;
      }
      return Clutter.EVENT_PROPAGATE;
    });
    hbox.is_mail_item = true;
    this.add_child(hbox);
  }

  open_mail() {
    for (let account of this.extension.account_map) {
      if (this.mail.account != account.uuid) continue;
      if (!Object.prototype.hasOwnProperty.call(account, 'webmail')) break;
      if (!account.webmail.length) break;
      Gio.AppInfo.launch_default_for_uri(account.webmail, null);
      return;
    }
    Utils.run_desktop_app(this.extension.mail_app);
  }

  on_hover() {
    GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
      if (this.options.show_date)
          this.date_lbl.visible = !this.hover;
      this.close_btn.visible = this.hover;
    });
  }

  vfunc_allocate(box, flags) {
    super.vfunc_allocate(box);
    GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
      if (this.options.show_avatars) {
        this.icon.width = this.get_preferred_size()[1] / (this.show_subject ? 1.33 : 1.66);
      }
    });
  }
});
