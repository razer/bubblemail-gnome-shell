![Screenshot](/screenshots/menu.png)

## Bubblemail gnome-shell extension indicator

Gnome shell extension indicator using [Bubblemail service](https://framagit.org/razer/bubblemail)

## Install from Git

You can build or install from git with [Meson](http://mesonbuild.com). You might need to install `libglib2.0-dev` and `meson` before:

```sh
git clone https://framagit.org/razer/bubblemail-gnome-shell.git
cd bubblemail-gnome-shell/
meson _build
ninja -C _build install-zip
```

## Meson build options for distribution packaging

You can build the extension for distribution, the resulting package will be in _build/_package folder:
```sh
git clone https://framagit.org/razer/bubblemail-gnome-shell.git
cd bubblemail-gnome-shell/
meson _build
ninja -C _build build
```
Available build options:

* **`-Dprefix=PATH`**

  Default is `/usr/local`

* **`-Dgnome_shell_libdir=PATH`**

  Default is `PREFIX/LIBDIR`, where `LIBDIR` is as defined by `--libdir`.

* **`-Dgsettings_schemadir=PATH`**

  Default is `PREFIX/DATADIR/glib-2.0/schemas`. If provided it will override the path when compiling and loading the GSchema so that the result is:

  ```sh
  GSETTINGS_SCHEMADIR/gschemas.compiled
  ```

* **`-Dpost_install=false`**

  Default is `false`. If `true` the script `meson/post-install.sh` will be run at the end of the `install` target. Currently, this compiles the GSchemas in `GSETTINGS_SCHEMADIR` using `glib-compile-schemas`.
