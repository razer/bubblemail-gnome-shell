#!/usr/bin/env python3
import os
from glob import glob

POT_FOLDER = './po'
POT_FILE = os.path.join(POT_FOLDER, 'bubblemail-gnome-shell.pot')
if not os.path.exists(POT_FOLDER):
    os.mkdir(POT_FOLDER)

def list_files(path, extension):
    file_list = filter(os.path.isfile, glob(os.path.join(path, '*')))
    return filter(lambda f: os.path.splitext(f)[1].lower() == f'.{extension}',
                  file_list)

def get_locale_map():
    with open('po/LINGUAS', 'r') as linguas_file:
        return filter(len, map(lambda l: l.strip(),
                               linguas_file.read().split()))

os.system(f'xgettext --from-code=UTF-8 --output={POT_FILE} src/*.js')

for locale in get_locale_map():
    if f'po/{locale}.po' not in list_files('po', 'po'):
        os.system(f'msginit --no-translator --input={POT_FILE} '
                  + f'--output-file={POT_FOLDER}/{locale}.po --locale={locale}')
    else:
        print(f'Updating po file for locale {locale}')
        os.system(f'msgmerge --update --backup=none {POT_FOLDER}/{locale}.po '
                  + f'{POT_FILE}')
